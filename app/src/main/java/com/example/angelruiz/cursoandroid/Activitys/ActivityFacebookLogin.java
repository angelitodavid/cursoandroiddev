package com.example.angelruiz.cursoandroid.Activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.angelruiz.cursoandroid.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

//class
public class ActivityFacebookLogin extends AppCompatActivity {

    //views
    private CircleImageView circleImageViewUser;
    private TextView tvEmailUser, tvInfoUser;
    private LoginButton loginButtonFacebook;
    private SignInButton btnLoginGoolge;
    private ProgressBar pbLoadLogInThird;

    //vars
    //manage response of call login
    private CallbackManager callbackManager;

    //login with facebook and google with firebase
    private FirebaseAuth firebaseAuthFacebook, firebaseAuthGoolge;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    public GoogleSignInClient googleSignInClient;
    private int CODE_RESULT_GOOGLE = 1;

    private static final String TAG_LOGIN_FACEBOOK = "TAG_LOGIN_FACEBOOK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_login);

        circleImageViewUser = findViewById(R.id.civ_image_user);
        tvInfoUser = findViewById(R.id.tv_info_user);
        tvEmailUser = findViewById(R.id.tv_email_user);
        loginButtonFacebook = findViewById(R.id.bt_login_facebook);
        pbLoadLogInThird = findViewById(R.id.pb_loadLogin_third);

        btnLoginGoolge = findViewById(R.id.btn_login_google);
        btnLoginGoolge.setSize(SignInButton.SIZE_WIDE);

        /*this comprovation serve only with login facebook withoth firebase auth
        //check if the user have session started
        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        if (!loggedOut){
            //of the user that have session started show his information through his token
            loadUserProfile(AccessToken.getCurrentAccessToken());
            //Glide.with(this).load(Profile.getCurrentProfile().getProfilePictureUri(100, 100)).into(circleImageViewUser);
            //tvEmailUser.setText(Profile.getCurrentProfile().getName());

        }else {
            circleImageViewUser.setImageResource(0);
            tvInfoUser.setText("");
            tvEmailUser.setText("");
            Toast.makeText(ActivityFacebookLogin.this, "Usuario no encontrado", Toast.LENGTH_SHORT).show();
        }*/

        callbackManager = CallbackManager.Factory.create();
        //set permissions for read data of user profile
        loginButtonFacebook.setReadPermissions(Arrays.asList("email", "public_profile"));
        responseLoginFacebook();

        //its connect with the api of google (gmail)
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build();

        //obtain the user through email and token
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        firebaseAuthFacebook = FirebaseAuth.getInstance();
        firebaseAuthGoolge = FirebaseAuth.getInstance();

        //open the intent implicit for show account
        btnLoginGoolge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoolge();
            }
        });
    }

    private void responseLoginFacebook(){

        //response about that result return the login
        loginButtonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //startActivity(new Intent(ActivityFacebookLogin.this, MapsActivity.class));
                //obtain the token of user de onSuccess
                handleFacebookAccessToken(loginResult.getAccessToken());
                loadUserProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(ActivityFacebookLogin.this, "Inicio de sesión cancelado", Toast.LENGTH_SHORT).show();
            }

            //manages error of red(connection) or response of server
            @Override
            public void onError(FacebookException error) {
                Toast.makeText(ActivityFacebookLogin.this, "Error de red", Toast.LENGTH_SHORT).show();
                Log.i(TAG_LOGIN_FACEBOOK, "Error: " + error.getMessage());
            }
        });

        loginFacebookFirebase();
    }

    //login with facebook through authentication firebase
    private void loginFacebookFirebase(){

        //check changes in authtentication
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null){

                    Uri photoUser = firebaseUser.getPhotoUrl();
                    String nameUser = firebaseUser.getDisplayName();
                    String emailUser = firebaseUser.getEmail();

                    //pass the data of user current if exists
                    showDataProfileFaceFire(photoUser, nameUser, emailUser);
                }else {
                    Toast.makeText(ActivityFacebookLogin.this, "Usuario no encontrado", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    //show data profile facebook in bottomsheetdialog
    private void showDataProfileFaceFire(Uri photoUser, String nameUser, String emailUser){

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(R.layout.layout_bottom_sheet_dialog_frb, findViewById(R.id.bottom_sheet_dialog_container));
        bottomSheetDialog.setContentView(view);

        CircleImageView civUserFrb = view.findViewById(R.id.civ_imageuser_frb);
        TextView tvInfoUserFrb = view.findViewById(R.id.tv_infouser_frb);
        TextView tvEmailUserFrb = view.findViewById(R.id.tv_emailuser_frb);
        Button btLogOutFaceFireAuth = view.findViewById(R.id.bt_login_facefireauth);

        Glide.with(this).load(photoUser).into(civUserFrb);
        tvInfoUserFrb.setText(nameUser);
        tvEmailUserFrb.setText(emailUser);
        btLogOutFaceFireAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                bottomSheetDialog.dismiss();
            }
        });
        bottomSheetDialog.show();
    }

    //obtain credential of facebook and access token
    private void handleFacebookAccessToken(AccessToken accessToken){

        //coplete the proces of authtentication
        AuthCredential authCredential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuthFacebook.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

               if (!task.isSuccessful()){
                   Toast.makeText(ActivityFacebookLogin.this, "Error al autenticar usuario", Toast.LENGTH_SHORT).show();
                   Log.i(TAG_LOGIN_FACEBOOK, "Error of authtentication with facebbok - firebase");
               }else {
                   Log.i(TAG_LOGIN_FACEBOOK, "success authtentication with facebbok - firebase");
               }
            }
        });
    }

    //open the window to access to google signin
    void signInGoolge(){
       Intent signIntent = googleSignInClient.getSignInIntent();
       startActivityForResult(signIntent, CODE_RESULT_GOOGLE);
    }

    //pass the results of the login with callbackManager (arrive results in gral of auth)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        //signin google
        if (requestCode == CODE_RESULT_GOOGLE){
            pbLoadLogInThird.setVisibility(View.VISIBLE);
            Task<GoogleSignInAccount> taskGoolge = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = taskGoolge.getResult(ApiException.class);
                if(account != null){
                   firebaseAuthWithGoolge(account);
                }
            } catch (ApiException e) {
                e.printStackTrace();
                Log.i(TAG_LOGIN_FACEBOOK, "Error account - SHA1" + e.getMessage());
            }
        }else {
            Toast.makeText(ActivityFacebookLogin.this, "Error inesperado", Toast.LENGTH_SHORT).show();
        }
    }

    //auth with google and firebase
    private void firebaseAuthWithGoolge(GoogleSignInAccount account) {

        AuthCredential authCredentialGoogle = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        firebaseAuthGoolge.signInWithCredential(authCredentialGoogle).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    pbLoadLogInThird.setVisibility(View.INVISIBLE);
                    FirebaseUser firebaseUserGoogle = firebaseAuthGoolge.getCurrentUser();
                    updateUiShowInfo(firebaseUserGoogle);

                    Log.i(TAG_LOGIN_FACEBOOK, "success signin");
                }else {

                    updateUiShowInfo(null);
                    Toast.makeText(ActivityFacebookLogin.this, "Cuenta no valida", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //show info of accoun google current
    private void updateUiShowInfo(FirebaseUser firebaseUserGoogle) {

        //GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (firebaseUserGoogle != null){

            String photoUserGoolge = String.valueOf(firebaseUserGoogle.getPhotoUrl());
            String nameUserGoogle = firebaseUserGoogle.getDisplayName();
            String emailUserGoogle = firebaseUserGoogle.getEmail();

            //pass data account google user to other activity
            Intent passDataUserGoole = new Intent(this, ActivityWelcomeSignInGoolge.class);
              passDataUserGoole.putExtra("photoUserGoolge", photoUserGoolge);
              passDataUserGoole.putExtra("nameUserGoogle", nameUserGoogle);
              passDataUserGoole.putExtra("emailUserGoogle", emailUserGoogle);
            startActivity(passDataUserGoole);
        }
    }

    //start to listen firebase auth
    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuthFacebook.addAuthStateListener(firebaseAuthListener);
        firebaseAuthGoolge.addAuthStateListener(firebaseAuthListener);
    }

    //stop of listen firebase auth
    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuthListener != null) {
            firebaseAuthFacebook.removeAuthStateListener(firebaseAuthListener);
            firebaseAuthGoolge.removeAuthStateListener(firebaseAuthListener);
        }
    }

    //show info basic of profile through his token with class GraphRequest
    private void loadUserProfile(AccessToken currentAccessToken){

        GraphRequest graphRequest = GraphRequest.newMeRequest(currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {

                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String id = object.getString("id");
                    String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

                    tvInfoUser.setText(String.format("%s %s", first_name, last_name));
                    tvEmailUser.setText(email);

                    Glide.with(getApplicationContext())
                            .load(image_url)
                            .into(circleImageViewUser);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //pass the fields to graphRequest to show
        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }
}
