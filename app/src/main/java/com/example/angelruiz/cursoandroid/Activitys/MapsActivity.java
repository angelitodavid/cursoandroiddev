package com.example.angelruiz.cursoandroid.Activitys;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.example.angelruiz.cursoandroid.Fragments.DialogFragmentShowInfoMarkerMap;
import com.example.angelruiz.cursoandroid.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

//class | onclick, drag, windowinfo, polyline, polygon event in markers interface
public class MapsActivity extends AppCompatActivity /*FragmentActivity*/ implements OnMapReadyCallback,
             GoogleMap.OnMarkerClickListener,GoogleMap.OnMarkerDragListener, GoogleMap.OnInfoWindowClickListener,
             View.OnClickListener, GoogleMap.OnPoiClickListener, GoogleMap.OnPolylineClickListener, GoogleMap.OnPolygonClickListener{

    //views
    Toolbar toolBarCustom;
    private Button btnChangeMapNormal, btnChangeMapHibrid, btnChangeMapSatelite;

    //objects
    private GoogleMap mMap;
    private Marker markerNaucalpan, markerShowDragg, markerNezahualcoyotl, markerPlaceSelectedDialog;
    Location locationCurrent;
    CameraPosition cameraPosition;
    FusedLocationProviderClient fusedLocationProviderClient;
    PlacesClient placesClient;
    String[] likelyPlaceNames, likelyPlaceAddresses, likelyPlacesPhone;
    List[] likelyPlacesAttributions;
    LatLng[] likelyPlacesLatLngs;

    //vars
    Boolean locationPermissionGranted = false;
    String mapsApiKey, markerSnippedPlaceSelected, markerSnippedPhonePlaceSelected;

    //consts, tags and latlng(positions)
    private static final String TAG_EXCEPTION_LOCATION = "TAG_EXCEPTION_LOCATION";
    private static final String TAG_PDI_ONCLICKLISTENER = "TAG_PDI_ONCLICKLISTENER";
    private static final int REQUEST_CODE = 1;
    private static final LatLng MARK_LATLNG_CITYMEXICO = new LatLng(19.4326009, -99.1333416);
    private static final LatLng MARK_LATLNG_ALVAROOBREGON = new LatLng(19.31814805, -99.27784436);
    private static final LatLng MARK_LATLNG_NAUCALPAN = new LatLng(19.4785796, -99.2328791);
    private static final LatLng MARK_LATLNG_TOLUCA = new LatLng(19.292545, -99.6569007);
    private static final LatLng MARK_LATLNG_NEZAHUALCOYOTL = new LatLng(19.4021151, -99.0170173);
    private static final String KEY_LOCATION = "SAVE_STATE_LOCATION";
    private static final String KEY_CAMERA_POSITION = "SAVE_STATE_CAMERA_POSITION";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        saveStateChangeConf(savedInstanceState);

        toolBarCustom = findViewById(R.id.actionBarCustom);
        btnChangeMapHibrid = findViewById(R.id.btn_changemap_normal);
        btnChangeMapNormal  = findViewById(R.id.btn_changemap_hibrid);
        btnChangeMapSatelite = findViewById(R.id.btn_changemap_satelite);
        setSupportActionBar(toolBarCustom);

        //inicialize places
        mapsApiKey = getString(R.string.maps_api_key);

        if (!Places.isInitialized()){
            Places.initialize(this, mapsApiKey);
        }

        placesClient = Places.createClient(this);

        //SupportMapFragment to show the map in API 12+.
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fmt_show_map);

           if(supportMapFragment != null){
               supportMapFragment.getMapAsync(this);
           }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getLocationPermission();
    }

    //show map when is ready
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        updateLocationUi();
        getDeviceLocation();
        showMarkersInPositions(googleMap);
        showPolylineCustom();
        showPolygonCustom();

        //listen the clicks, drag, windowinfo, PDI of each marker in specific
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMarkerDragListener(this);
        googleMap.setOnInfoWindowClickListener(this);
        googleMap.setOnPoiClickListener(this);
        googleMap.setOnPolylineClickListener(this);
        btnChangeMapHibrid.setOnClickListener(this);
        btnChangeMapNormal.setOnClickListener(this);
        btnChangeMapSatelite.setOnClickListener(this);
    }

    //recove state of activity
    private void saveStateChangeConf(Bundle savedInstanceState){

        //recove position camera and location
        if (savedInstanceState != null){
            locationCurrent = savedInstanceState.getParcelable(KEY_LOCATION);
            cameraPosition  = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);

        }else{
            Toast.makeText(this, "Cambio de camara no guardado.", Toast.LENGTH_SHORT).show();
        }
    }

    //save state activity ante changes of config
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        //save position camera and location
        if (mMap != null){
            outState.putParcelable(KEY_LOCATION, locationCurrent);
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
        }
    }

    //obtain permission of location
    private void getLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            locationPermissionGranted = true;
        }else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        }
    }

    //check if permission are accepted
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    locationPermissionGranted = true;
                }else {
                    locationPermissionGranted = false;
                }
            break;
        }
        updateLocationUi();
    }

    //update location current if permission are accepted
    private void updateLocationUi() {

        if (mMap != null){
            try {
                if (locationPermissionGranted){
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                }else {
                    mMap.setMyLocationEnabled(false);
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    getLocationPermission();
                }
            }catch (SecurityException e){
                Log.e(TAG_EXCEPTION_LOCATION, e.getMessage());
            }
        }else {
            Toast.makeText(this, "Error al mostrar mapa.", Toast.LENGTH_SHORT).show();
        }
    }

    //obtain location current
    private void getDeviceLocation() {
        try {
           if (locationPermissionGranted){
               Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
               locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                   @Override
                   public void onComplete(@NonNull Task<Location> task) {
                       if (task.isSuccessful()){
                           locationCurrent = task.getResult();

                           if (locationCurrent != null){
                               mMap.moveCamera(CameraUpdateFactory
                                       .newLatLngZoom(new LatLng(locationCurrent.getLatitude(),
                                                                 locationCurrent.getLongitude()), 10));
                           }else {
                               mMap.getUiSettings().setMyLocationButtonEnabled(false);
                               Log.e(TAG_EXCEPTION_LOCATION, String.valueOf(task.getException()));
                           }
                       }
                   }
               });
           }else {
             getLocationPermission();
           }
        }catch (SecurityException e){
            Log.e(TAG_EXCEPTION_LOCATION, e.getMessage());
        }
    }

    //onclick event in markers
    @Override
    public boolean onMarkerClick(Marker marker) {
        //show title, lat, lng of this marker
        if (marker.equals(markerNaucalpan)){
            String titleMarker = marker.getTitle().toUpperCase();
            String lat = Double.toString(marker.getPosition().latitude);
            double lng = marker.getPosition().longitude;
            Toast.makeText(this, titleMarker + ": lat: "+ lat +" - lng: "+lng, Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    //its execute to the start the dragg
    @Override
    public void onMarkerDragStart(Marker marker) {
        if (marker.equals(markerShowDragg)){
            Toast.makeText(this, "start a dragg", Toast.LENGTH_SHORT).show();
        }
    }

    //it execute while it is dragging
    @Override
    public void onMarkerDrag(Marker marker) {
        //show in toolbar lat, lng of place where it dragg the marker
        if (marker.equals(markerShowDragg)){
            String newTitleToolBar = String.format(Locale.getDefault(),"%1$.2f, %2$.2f",
                    marker.getPosition().latitude,
                    marker.getPosition().longitude);

            setTitle(newTitleToolBar);
        }
    }

    //it execute when it stop of dragg (drop)
    @Override
    public void onMarkerDragEnd(Marker marker) {
       if (marker.equals(markerShowDragg)){
           setTitle(getResources().getString(R.string.title_activity_maps));
           Toast.makeText(this, "finish the dragg", Toast.LENGTH_SHORT).show();
       }
    }

    //show dialog info in marker
    @Override
    public void onInfoWindowClick(Marker marker) {
       if (marker.equals(markerNezahualcoyotl)){
           dialogFragmentMarkerInfo(marker.getTitle(), getResources().getString(R.string.show_info_marker_neza));

       }else if (marker.equals(markerPlaceSelectedDialog)){
           dialogFragmentMarkerInfo(marker.getTitle(), markerSnippedPlaceSelected +"\n"+ "Teléfono: " + markerSnippedPhonePlaceSelected);
       }
    }

    //onclick listener on PDI, for show his info basic
    @Override
    public void onPoiClick(PointOfInterest pointOfInterest) {
        dialogFragmentMarkerInfo(pointOfInterest.name, pointOfInterest.placeId);

        Log.i(TAG_PDI_ONCLICKLISTENER, " \n Name PDI: " + pointOfInterest.name +
                " \n Place id: " + pointOfInterest.placeId +
                " \n Latitud: " + pointOfInterest.latLng.latitude +
                " \n Longitud: " + pointOfInterest.latLng.longitude);
    }

    //show info marker in dialog fragment
    private void dialogFragmentMarkerInfo(String titleMarker, String snippedMarker){
        DialogFragmentShowInfoMarkerMap
                .newInstance(titleMarker, snippedMarker)
                .show(getSupportFragmentManager(), null);
    }

    //create markers custom
    private void showMarkersInPositions(GoogleMap googleMapParam) {
        //Add a marker in cdmx and move the camera
        //LatLng cityMexico = new LatLng(19.4326009, -99.1333416);
        mMap.addMarker(new MarkerOptions()
                .position(MARK_LATLNG_CITYMEXICO)
                .title("Ciudad de México")
                .snippet("Es el centro del pais"));

        //marker in alvaro obregron
        //LatLng alvaroObregon = new LatLng(19.31814805, -99.27784436);
        mMap.addMarker(new MarkerOptions()
                .position(MARK_LATLNG_ALVAROOBREGON)
                .title("Alcaldia Alvaro Obregón")
                .snippet("En Ciudad de México")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_snail)));

        //marker in naucalpan edomex
        //LatLng markNaupn = new LatLng(19.4785796, -99.2328791);
        markerNaucalpan = googleMapParam.addMarker(new MarkerOptions()
                .position(MARK_LATLNG_NAUCALPAN)
                .title("Naucalpan de Juarez")
                .snippet("En Estado de México")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        //masrker show lat, lng drag and drop
        LatLng markSwLatLngDrag = new LatLng(19.292545, -99.6569007);
        markerShowDragg = googleMapParam.addMarker(new MarkerOptions()
                .position(markSwLatLngDrag)
                .title("Toluca")
                .snippet("Muestra lat y long al arrastrarlo")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_touch_app))
                .draggable(true));

        LatLng markNezhtl = new LatLng(19.4021151, -99.0170173);
        markerNezahualcoyotl = googleMapParam.addMarker(new MarkerOptions()
                .position(markNezhtl)
                .title("Nezahualcóyotl")
                .snippet(getString(R.string.marker_snipped_details))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        //position each marker to the center and do zoom
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(cityMexico));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MARK_LATLNG_CITYMEXICO, 10)); //start with zoom of 10
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    //create polyline with 3 positions
    private void showPolylineCustom() {
        PolylineOptions polylineOptions = new PolylineOptions()
                .clickable(true)
                .add(MARK_LATLNG_ALVAROOBREGON,
                     MARK_LATLNG_NAUCALPAN, MARK_LATLNG_TOLUCA)
                .width(5)
                .color(Color.BLUE)
                .geodesic(true);

        mMap.addPolyline(polylineOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MARK_LATLNG_CITYMEXICO, 4));

        /*polyline simplified, example of the doc
        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(new LatLng(-35.016, 143.321),
                        new LatLng(-34.747, 145.592),
                        new LatLng(-34.364, 147.891),
                        new LatLng(-33.501, 150.217),
                        new LatLng(-32.306, 149.248),
                        new LatLng(-32.491, 147.309)));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-23.684, 133.903), 4));*/
    }

    //onclick listener, on polyline for show details
    @Override
    public void onPolylineClick(Polyline polyline) {
        Toast.makeText(this, "polilinea: " + polyline.getPoints(), Toast.LENGTH_SHORT).show();
    }

    //create polygon in map
    public void showPolygonCustom(){
        PolygonOptions polygonOptions = new PolygonOptions()
                .clickable(true)
                .add(MARK_LATLNG_CITYMEXICO, MARK_LATLNG_ALVAROOBREGON,
                     MARK_LATLNG_NEZAHUALCOYOTL, MARK_LATLNG_CITYMEXICO)
                .strokeWidth(5)
                .strokeColor(Color.RED)
                .fillColor(Color.YELLOW);
        mMap.addPolygon(polygonOptions);
    }

    //onclick listener, on polygon for show details
    @Override
    public void onPolygonClick(Polygon polygon) {
        Toast.makeText(this, "poligono: " + polygon.getPoints(), Toast.LENGTH_SHORT).show();
    }

    //show current places
    public void showCurrentPlaces(){

        if (mMap != null){
            if (locationPermissionGranted){
                //data to show of the places through place.field
                List<Place.Field> placesFields = Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

                FindCurrentPlaceRequest findCurrentPlaceRequest = FindCurrentPlaceRequest.newInstance(placesFields);
                //obtain places current to my location
                @SuppressWarnings("MissingPermission")
                final Task<FindCurrentPlaceResponse> placeRequestTask = placesClient.findCurrentPlace(findCurrentPlaceRequest);

                placeRequestTask.addOnCompleteListener(new OnCompleteListener<FindCurrentPlaceResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindCurrentPlaceResponse> task) {
                        if (task.isSuccessful() && task.getResult() != null){
                            //obtain places
                            FindCurrentPlaceResponse likelyPlaces = task.getResult();

                            //check quantity of places
                            int count;
                            if (likelyPlaces.getPlaceLikelihoods().size() < 5){
                                count = likelyPlaces.getPlaceLikelihoods().size();
                            }else {
                                count = 5;
                            }

                            //position arrays
                            int i = 0;

                            //inicialize arrays size
                            likelyPlaceNames = new String[count];
                            likelyPlaceAddresses = new String[count];
                            likelyPlacesPhone = new String[count];
                            likelyPlacesAttributions = new List[count];
                            likelyPlacesLatLngs = new LatLng[count];

                            //fill arrays with data of likely places nearby
                            for (PlaceLikelihood placeLikelihood : likelyPlaces.getPlaceLikelihoods()){
                                 likelyPlaceNames[i] = placeLikelihood.getPlace().getName();
                                 likelyPlaceAddresses[i] = placeLikelihood.getPlace().getAddress();
                                 likelyPlacesPhone[i] = placeLikelihood.getPlace().getPhoneNumber();
                                 likelyPlacesAttributions[i] = placeLikelihood.getPlace().getAttributions();
                                 likelyPlacesLatLngs[i] = placeLikelihood.getPlace().getLatLng();

                                 i++;
                                 if (i > (count -1)){
                                     break;
                                 }
                            }

                            showPlacesDialog();
                        }else {
                            Log.i(TAG_EXCEPTION_LOCATION, Objects.requireNonNull(task.getException()).toString());
                        }
                    }
                });

            }else {
                //show marker by default if an error happens
                mMap.addMarker(new MarkerOptions()
                        .position(MARK_LATLNG_CITYMEXICO)
                        .title("Error.")
                        .snippet("Marcador no disponible!"));

                getLocationPermission();
            }

        }else {
            Toast.makeText(this, "Error al mostrar detalles.", Toast.LENGTH_SHORT).show();
        }
    }

    //show dialog with places currents
    public void showPlacesDialog(){
        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int itemPosition) {
               //obtain data of place according the item selected to create the marker
               String markerTitlePlaceSelected = likelyPlaceNames[itemPosition];
               markerSnippedPlaceSelected = likelyPlaceAddresses[itemPosition];
               markerSnippedPhonePlaceSelected = likelyPlacesPhone[itemPosition];
               LatLng markerLatLngSelected = likelyPlacesLatLngs[itemPosition];

               /*if (likelyPlacesAttributions[watch] != null){
                    markerSnipped = markerSnipped + "\n" + likelyPlacesAttributions[watch];
               }*/

               //create a marker with latlng obtained of the dialog item selected
               markerPlaceSelectedDialog = mMap.addMarker(new MarkerOptions()
                                           .position(markerLatLngSelected)
                                           .title(markerTitlePlaceSelected)
                                           .snippet(getString(R.string.marker_snipped_details)));
            }
        };

        //show alertdialog with names of likely places
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Lugares actuales.")
                .setItems(likelyPlaceNames, dialogListener)
                .show();
    }

    //change type of map though toolbar
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_changemap_normal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                showCurrentPlaces();
                break;

            case R.id.btn_changemap_hibrid:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;

            case R.id.btn_changemap_satelite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
        }
    }
}

/*
  Manipulates the map once available.
  This callback is triggered when the map is ready to be used.
  This is where we can add markers or lines, add listeners or move the camera. In this case,
  we just add a marker near Sydney, Australia.
  If Google Play services is not installed on the device, the user will be prompted to install
  it inside the SupportMapFragment. This method will only be triggered once the user has
  installed Google Play services and returned to the app.
  PDI, points of interest, shown on the map, how parcks, schools, government buildings and more, the user may be
  interested in his info and details, (here we can trace a route, distance and time, from where it is
  if I wanted to go there the user).
  we can also disable(hide) the PDI of the map.
  we can also obtain info of location current with his onclick listener.
  we can also do view street, and do intents with URL of google maps, to send to the user a google maps
*/