package com.example.angelruiz.cursoandroid.Activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.angelruiz.cursoandroid.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityWelcomeSignInGoolge extends AppCompatActivity implements View.OnClickListener {

    //views
    private CircleImageView civImageUserGoogle;
    private TextView tvInfoUserGoolge, tvEmailUserGoogle;
    private Button btnSignInGoolge;

    //vars
    GoogleSignInClient googleSignInClient;
    private Uri photoUserGoolge;
    private String nameUserGoogle;
    private String emailUserGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_sign_in_goolge);

        civImageUserGoogle = findViewById(R.id.civ_imageuser_google);
        tvInfoUserGoolge = findViewById(R.id.tv_infouser_google);
        tvEmailUserGoogle = findViewById(R.id.tv_emailuser_google);
        btnSignInGoolge = findViewById(R.id.btn_signin_google);
        btnSignInGoolge.setOnClickListener(this);

        //receibe data of activityfacebooklogin of user account google
        Intent receiveDataUserGoogle = this.getIntent();
        if (receiveDataUserGoogle != null){
            photoUserGoolge = Uri.parse(getIntent().getStringExtra("photoUserGoolge"));
            nameUserGoogle = getIntent().getStringExtra("nameUserGoogle");
            emailUserGoogle = getIntent().getStringExtra("emailUserGoogle");

            showDataUserGoolge(photoUserGoolge, nameUserGoogle, emailUserGoogle);
        }

        //its connect with the api of google (gmail) - here serve to logout
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
    }

    //show information of account user current google
    private void showDataUserGoolge(Uri photoUserGoolge, String nameUserGoogle, String emailUserGoogle){
        Glide.with(this).load(photoUserGoolge).into(civImageUserGoogle);
        tvInfoUserGoolge.setText(nameUserGoogle);
        tvEmailUserGoogle.setText(emailUserGoogle);
    }

    //logout of google and firebase
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_signin_google){
            FirebaseAuth.getInstance().signOut();
            googleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    goScreenLoginFacebookGoogle();
                }
            });
        }
    }

    //retun to screen login fbk gle
    private void goScreenLoginFacebookGoogle() {
        Intent intent = new Intent(this, ActivityFacebookLogin.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
