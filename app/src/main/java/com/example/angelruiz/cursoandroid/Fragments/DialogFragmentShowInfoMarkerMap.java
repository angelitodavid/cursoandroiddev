package com.example.angelruiz.cursoandroid.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class DialogFragmentShowInfoMarkerMap extends DialogFragment {

    private static final String ARGUMENT_TITLE = "TILTE";
    private static final String ARGUMENT_SNIPPET_FULL = "SNIPPET_FULL";

    private String titleDialog, snippetFullDialog;

    public static DialogFragmentShowInfoMarkerMap newInstance(String titleDialog, String snippetFullDialog){

        DialogFragmentShowInfoMarkerMap dialogFragmentShowInfoMarkerMap = new DialogFragmentShowInfoMarkerMap();

        Bundle passInfo = new Bundle();
        passInfo.putString(ARGUMENT_TITLE, titleDialog);
        passInfo.putString(ARGUMENT_SNIPPET_FULL, snippetFullDialog);
        dialogFragmentShowInfoMarkerMap.setArguments(passInfo);

        return dialogFragmentShowInfoMarkerMap;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle receiveArgs = getArguments();
        if (receiveArgs != null){
            titleDialog = receiveArgs.getString(ARGUMENT_TITLE);
            snippetFullDialog = receiveArgs.getString(ARGUMENT_SNIPPET_FULL);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle(titleDialog)
                .setMessage(snippetFullDialog)
                .create();
    }
}
