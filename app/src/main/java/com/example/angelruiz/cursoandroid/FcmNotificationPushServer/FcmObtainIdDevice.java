package com.example.angelruiz.cursoandroid.FcmNotificationPushServer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.angelruiz.cursoandroid.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

//extends of this class to use the service fcm(firebase cloud messagin)
public class FcmObtainIdDevice extends FirebaseMessagingService {

    private static final String NOTIFICATION_RESPONSE = "NOTIFICATION_RESPONSE";

    //obtain token of device for receive notif push
    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);

        sendTokenToServer(token);

    }

    private void sendTokenToServer(String token) {
        Log.i(NOTIFICATION_RESPONSE, "tokenid_device: " + token);
    }

    //receive notification push of frb or own server
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        //obtain data of notification by default
        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String message = remoteMessage.getNotification().getBody();
            String actionClickNotifPush = remoteMessage.getNotification().getClickAction();

            sendNotificationByTokenDevice(title, message, actionClickNotifPush);
        }

        //obtain data of notification by key value from frb
        if (remoteMessage.getData().size() > 0) {
            Log.i(NOTIFICATION_RESPONSE, "Title: " + remoteMessage.getData().get("titulo"));
        }
    }

    //create notif push for be shown
    private void sendNotificationByTokenDevice(String title, String message, String actionClickNotifPush){

        Intent intent = new Intent(actionClickNotifPush);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        //String channelId = getString(R.string.defa)
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "N")
            .setSmallIcon(R.drawable.ic_snail)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel =
                    new NotificationChannel("N", "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }
}
//eHif6KxnQjQ:APA91bESt-eAPwzZb1fwipNph0eUn3Jd1EXD_Y0V_oU4v9pqqWPQtCeBStDm0GAu59CrYxUMULU2Pcf82BPcTNgUHkl5K0QZ9DGRZlPcgkG-AAFFQfOC7cCUX_beODRQL1w8TSlJz0UX