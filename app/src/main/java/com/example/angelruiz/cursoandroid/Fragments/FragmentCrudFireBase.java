package com.example.angelruiz.cursoandroid.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.angelruiz.cursoandroid.Adapters.AdapterCrudFireBaseRV;
import com.example.angelruiz.cursoandroid.Arrays.ArrayCrudFirebase;
import com.example.angelruiz.cursoandroid.InterfazAPI_REST.IOnClickItemRecyclerView;
import com.example.angelruiz.cursoandroid.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.app.Activity.RESULT_OK;

//ET -> edittext
//class
public class FragmentCrudFireBase extends Fragment {

    //vars
    private final static String TAG_ARRAY_FIREBASE = "TAG_ARRAY_FIREBASE";
    private final static String TAG_RESPONSE_STORAGE = "TAG_RESPONSE_STORAGE";
    private final static String TAG_RESPONSE_NOTIFPUSH = "TAG_RESPONSE_NOTIFPUSH";

    private static final int SELECT_PICTURE = 100;
    private Context context;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private String userName, userLastName, userEmail, userPassword, result, uriImgePath;
    private Uri imageSelectedGallery;
    //this list should save values of type ArrayCrudFirebase(pojo)
    private List<ArrayCrudFirebase> arrayCrudFirebaseList;
    private AdapterCrudFireBaseRV adapterCrudFireBaseRV;
    private ArrayCrudFirebase arrayCrudItemSelected;
    private Bitmap compressedImageBitmap;
    private byte[] bytesImageCompress;
    private Bitmap bitmapDisplayImage;

    //views
    View view;
    private Toolbar toolbarCustom;
    private EditText etUserNameFrb, etUserLastNameFrb, etUserEmailFrb, etUserPasswordFrb;
    private Button btnSelectImage;
    private RecyclerView rvShowUsersFrb;
    private ProgressBar pbLoadUploadImage;
    private TextView tvMessageUploadImage;

    //builder
    public FragmentCrudFireBase() {
        // Required empty public constructor
    }

    //inicialize objects and vars
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        arrayCrudFirebaseList = new ArrayList<>();
        inicializeFireBase();
        showUserDataService();
        showTokenIdDevice();
    }

    //show data services frb in background
    private void showUserDataService(){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                showUserData();
            }
        });
        executorService.shutdown();
    }

    //obtain tokenid of the device
    private void showTokenIdDevice() {

        FirebaseInstanceId
                .getInstance()
                .getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (task.isSuccessful()){
                            if (task.getResult() != null){
                                String obtainToken = task.getResult().getToken();
                                Log.i(TAG_RESPONSE_NOTIFPUSH, obtainToken);
                            }

                        }else {
                            Log.i(TAG_RESPONSE_NOTIFPUSH, "getInstanceId failed: ", task.getException());
                        }
                    }
                });
    }

    //connection to firebase database
    private void inicializeFireBase(){
        FirebaseApp.initializeApp(context);
        firebaseDatabase = FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true); not call here the percsistence of data
        databaseReference = firebaseDatabase.getReference(); //make reference to the node main of frb -> curso-android-11cc5
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference().child("ImageFolder");
    }

    //show data from fire base of the node "UserData" in RV
    private void showUserData() {

        databaseReference.child("UserData").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //dataSnapshot is all object UserData
                if(dataSnapshot.exists()){
                    //first clear list to not show duplicate data
                    arrayCrudFirebaseList.clear();

                    for (DataSnapshot objectDataSnapshot : dataSnapshot.getChildren()) {
                         ArrayCrudFirebase arrayCrudDataSnapshot = objectDataSnapshot.getValue(ArrayCrudFirebase.class);

                        if (arrayCrudDataSnapshot != null){
                            arrayCrudDataSnapshot.setKey(objectDataSnapshot.getKey());
                            arrayCrudFirebaseList.add(arrayCrudDataSnapshot);

                            //onclick to RV recovering position of item selected
                            adapterCrudFireBaseRV = new AdapterCrudFireBaseRV(context, arrayCrudFirebaseList, new IOnClickItemRecyclerView() {
                                @Override
                                public void setOnClickItemRecylcerView(int position) {
                                    showDataInEditText(position);
                                }
                            });
                            rvShowUsersFrb.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            rvShowUsersFrb.setAdapter(adapterCrudFireBaseRV);

                        }else {
                            Log.i(TAG_ARRAY_FIREBASE, "snap_shot: without data");
                        }
                    }

                    adapterCrudFireBaseRV.notifyDataSetChanged();

                }else {
                    Toast.makeText(context, "Sin registros", Toast.LENGTH_SHORT).show();
                }

                /*ckeck if arrayCrudFirebases has data
                for (ArrayCrudFirebase x: arrayCrudFirebases) {
                    Log.i(TAG_ARRAY_FIREBASE, x.getFrbUserName());
                }

                for (int i = 0; i < arrayCrudFirebases.size(); i++) {
                    Log.i(TAG_ARRAY_FIREBASE, arrayCrudFirebases.get(i).getFrbUserName());
                }*/
            }

            //manages update of data
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               Log.i(TAG_ARRAY_FIREBASE, "Error update" + databaseError.toException());
            }
        });
    }

    //show all the data of the item RV selected in ET
    private void showDataInEditText(int position){
        arrayCrudItemSelected = arrayCrudFirebaseList.get(position);
        //etUserNameFrb.setText(arrayCrudFirebaseList.get(position).getFrbUserName());

        etUserNameFrb.setText(arrayCrudItemSelected.getFrbUserName());
        etUserLastNameFrb.setText(arrayCrudItemSelected.getFrbUserLastName());
        etUserEmailFrb.setText(arrayCrudItemSelected.getFrbUserEmail());
        etUserPasswordFrb.setText(arrayCrudItemSelected.getFrbUserPassword());
    }

    //inicialize views
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_crud_fire_base, container, false);

        toolbarCustom = view.findViewById(R.id.actionbar_custom_frb);
        setToolBarFragment();
        etUserNameFrb = view.findViewById(R.id.et_user_name_frb);
        etUserLastNameFrb = view.findViewById(R.id.et_user_lastname_frb);
        etUserEmailFrb = view.findViewById(R.id.et_user_email_frb);
        etUserPasswordFrb = view.findViewById(R.id.et_user_password_frb);
        btnSelectImage = view.findViewById(R.id.btn_select_image);
        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageGallery();
            }
        });
        rvShowUsersFrb = view.findViewById(R.id.rv_show_users_frb);
        pbLoadUploadImage = view.findViewById(R.id.pb_loadupload_image);
        tvMessageUploadImage = view.findViewById(R.id.tv_messajeupload_image);

        return view;
    }

    //initialization final
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    //toolbar custom menu for this fmt
    private void setToolBarFragment(){
        setHasOptionsMenu(true);
        toolbarCustom.inflateMenu(R.menu.menu_crud_firebase);
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        if(activity != null){
           activity.setSupportActionBar(toolbarCustom);

           if (activity.getSupportActionBar() != null){
               activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
               activity.getSupportActionBar().setTitle("Fire base");
           }
        }
    }

    //inflate the menu of the toolbar
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
        MenuItem settingsItem = menu.findItem(R.id.action_settings);
        //settingsItem.setVisible(false); remove 3 dots of toolbar
        menuInflater.inflate(R.menu.menu_crud_firebase, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    //events to each item of the menu toolbar
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        userName = etUserNameFrb.getText().toString();
        userLastName = etUserLastNameFrb.getText().toString();
        userEmail = etUserEmailFrb.getText().toString();
        userPassword = etUserPasswordFrb.getText().toString();

        switch(item.getItemId()){
            case R.id.menu_save_firebase:
                uploadImgFirebaseStorage();
                break;

            case R.id.menu_update_firebase:
                updateDataFireBase();
                break;

            case R.id.menu_delete_firebase:
                deleteDataFireBase();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //select image of gallery
    @SuppressLint("IntentReset")
    private void selectImageGallery() {
        Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intentGallery.setType("image/*");
        startActivityForResult(Intent.createChooser(intentGallery, "Seleccionar"), SELECT_PICTURE);
    }

    //recover and compress image selected
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){
            if (data != null && requestCode == SELECT_PICTURE) {
                imageSelectedGallery = data.getData();

                try {
                    compressedImageBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageSelectedGallery);
                    compressedImageBitmap = compressImage(compressedImageBitmap, 100, 100);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedImageBitmap != null){
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    compressedImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    showBottomSdImageSelected(compressedImageBitmap);
                    bytesImageCompress = byteArrayOutputStream.toByteArray();

                }else {
                    Toast.makeText(context, "Archivo no soportado.", Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Log.i("Error", "Error inseperado");
        }

        /*try {
             //method for compress image of gallery selected using zolery librery
               uriImgePath = getRealPathFromUri(imageSelectedGallery);
                File file = new File(uriImgePath);
                    if(file.exists() && file.canRead()) {
                        compressedImageFile = new Compressor(context)
                                .setMaxWidth(100)
                                .setMaxHeight(100)
                                .setQuality(50)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .compressToFile(file);

                    }else{
                        Log.i("Error", "File not soported");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
    }

    //compress image selected of gallery
    private Bitmap compressImage(Bitmap compressedImageFile, float widthNew, float heightNew) {

        int widthCurrent = compressedImageFile.getWidth();
        int heightCurrent = compressedImageFile.getHeight();

        if (widthCurrent > widthNew || heightCurrent > heightNew){
            float scaleWeight = widthNew / widthCurrent;
            float scaleHeight = heightNew / heightCurrent;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleWeight, scaleHeight);

            return Bitmap.createBitmap(compressedImageFile, 0, 0, widthCurrent, heightCurrent, matrix, false);
        }else {
            return compressedImageFile;
        }
    }


    //obtain path of image
    private String getRealPathFromUri(Uri uriImagePath){

        String[] projection = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(context, uriImagePath, projection, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null){
            int columnIdx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(columnIdx);

            cursor.close();
        }
        return result;
    }

    //display image selected in bottom sheet dialog
    private void showBottomSdImageSelected(Bitmap receiveImageSelected) {

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        bottomSheetDialog.setContentView(R.layout.layout_bottom_sheet_dialog_frs);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        if (receiveImageSelected != null){
            ImageView ivImageSelected = bottomSheetDialog.findViewById(R.id.iv_image_selected);
            Button btnConfirmImage = bottomSheetDialog.findViewById(R.id.btn_comfirm_image);
            Button btnCancelImage = bottomSheetDialog.findViewById(R.id.btn_cancel_image);

            if (ivImageSelected != null && btnCancelImage != null && btnConfirmImage != null){
                ivImageSelected.setImageBitmap(receiveImageSelected);

                btnCancelImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.cancel();
                        selectImageGallery();
                    }
                });

                btnConfirmImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                    }
                });
            }

        }else {
            Toast.makeText(context, "Error al seleccionar imagen", Toast.LENGTH_SHORT).show();
        }

        bottomSheetDialog.show();
    }

    //save register in db firebase
    private void saveDataFireBase(Uri uriImgStorage) {
        if (uriImgStorage != null){

            ArrayCrudFirebase arrayCrudSave = new ArrayCrudFirebase();
            arrayCrudSave.setFrbUserId(UUID.randomUUID().toString());
            arrayCrudSave.setFrbUserName(userName);
            arrayCrudSave.setFrbUserLastName(userLastName);
            arrayCrudSave.setFrbUserEmail(userEmail);
            arrayCrudSave.setFrbUserPassword(userPassword);
            arrayCrudSave.setFrbUserImageUrl(uriImgStorage.toString());

            //inside of databaseReference create a node child with method child
            databaseReference
                    .child("UserData")
                    .child(arrayCrudSave.getFrbUserId()) //in this child id, it saved the registers with setValue()
                    .setValue(arrayCrudSave)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(context, "Usuario guardado", Toast.LENGTH_SHORT).show();
                            cleanEditText();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(context, "Usuario no guardado", Toast.LENGTH_SHORT).show();
                        }
                    });
        }else {
            Toast.makeText(context, "Error al cargar imagen", Toast.LENGTH_SHORT).show();
        }
    }

    //upload image to firebase storage and firebase database
    private void uploadImgFirebaseStorage(){

        if (imageSelectedGallery != null && !validateEditTextNotEmpty()) {

            final StorageReference reference = storageReference.child("image" + UUID.randomUUID().toString());
            UploadTask uploadTask = reference.putBytes(bytesImageCompress);

            Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {

                    if (!task.isSuccessful()) {
                        //throw Objects.requireNonNull(task.getException());
                        Log.i(TAG_RESPONSE_STORAGE, "ERROR: IMG NOT UPLOADED");
                    }
                    return reference.getDownloadUrl();
                }

            //image upload success and getting his link
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                   if (task.isSuccessful()){
                       Uri downloadUri = task.getResult(); //link of image to save in db frb

                       saveDataFireBase(downloadUri);
                       Toast.makeText(context, "¡Imagen guardada!", Toast.LENGTH_SHORT).show();
                       imageSelectedGallery = null;

                       if (downloadUri != null) {
                           //String uriImage = downloadUri.toString().substring(0, downloadUri.toString().indexOf("&token"));
                           String uriImage = downloadUri.toString();
                           Log.i(TAG_RESPONSE_STORAGE, uriImage);
                       }

                   }else {
                       Toast.makeText(context, "¡Imagen no guardada!", Toast.LENGTH_SHORT).show();
                   }
                }
            });

            //show progress of load upload image
            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    int progressTotal = (int) progress;

                    pbLoadUploadImage.setVisibility(View.VISIBLE);
                    tvMessageUploadImage.setVisibility(View.VISIBLE);
                    pbLoadUploadImage.setProgress(progressTotal);

                    if (progressTotal >= 100){
                        pbLoadUploadImage.setVisibility(View.INVISIBLE);
                        tvMessageUploadImage.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }else {
            Toast.makeText(context, "¡Imagen no encontrada!...", Toast.LENGTH_SHORT).show();
        }
    }

    //update register by id in db firebase
    private void updateDataFireBase() {

        if (!validateEditTextNotEmpty() && arrayCrudItemSelected.getFrbUserId() != null){
            ArrayCrudFirebase arrayCrudUpdate = new ArrayCrudFirebase();

            arrayCrudUpdate.setFrbUserId(arrayCrudItemSelected.getFrbUserId()); //the same as the image url
            arrayCrudUpdate.setFrbUserName(userName.trim());
            arrayCrudUpdate.setFrbUserLastName(userLastName.trim());
            arrayCrudUpdate.setFrbUserEmail(userEmail.trim());
            arrayCrudUpdate.setFrbUserPassword(userPassword.trim());
            arrayCrudUpdate.setFrbUserImageUrl(arrayCrudItemSelected.getFrbUserImageUrl()); //save your same url to avoid losing the image

            databaseReference
                    .child("UserData")
                    .child(arrayCrudUpdate.getFrbUserId())
                    //updateChildren(map) -> serves too to update the atributes of the node
                    .setValue(arrayCrudUpdate)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(context, "Registro actualizado", Toast.LENGTH_SHORT).show();
                            cleanEditText();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(context, "Registro no actualizado", Toast.LENGTH_SHORT).show();
                        }
                    });
        }else {
            Toast.makeText(context, "Seleccionar registro", Toast.LENGTH_SHORT).show();
        }
    }

    //delete register by id in db firebase
    private void deleteDataFireBase() {

        if (validateEditTextNotEmpty()){
            Toast.makeText(context, "Seleccionar registro", Toast.LENGTH_SHORT).show();

        }else {

            ArrayCrudFirebase arrayCrudDelete = new ArrayCrudFirebase();
            arrayCrudDelete.setFrbUserId(arrayCrudItemSelected.getFrbUserId());
            databaseReference
               .child("UserData")
               .child(arrayCrudDelete.getFrbUserId())
               .removeValue()
               .addOnSuccessListener(new OnSuccessListener<Void>() {
                   @Override
                   public void onSuccess(Void aVoid) {
                       deleteImageStorage();
                       Toast.makeText(context, "Registro eliminado", Toast.LENGTH_SHORT).show();
                       cleanEditText();
                   }
               })
               .addOnFailureListener(new OnFailureListener() {
                   @Override
                   public void onFailure(@NonNull Exception e) {
                       Toast.makeText(context, "Registro no eliminado", Toast.LENGTH_SHORT).show();
                   }
               });
        }
    }

    //delete image of storage firebase
    private void deleteImageStorage() {

        StorageReference storRefeDeleteImage = firebaseStorage.getReferenceFromUrl(arrayCrudItemSelected.getFrbUserImageUrl());
        storRefeDeleteImage.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.i(TAG_RESPONSE_STORAGE, "image deleted" );
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i(TAG_RESPONSE_STORAGE, "image not deleted" );
            }
        });
    }

    //validate that the ET are not empty
    private Boolean validateEditTextNotEmpty(){
        if(userName.isEmpty() || userLastName.isEmpty() || userEmail.isEmpty() || userPassword.isEmpty()){
           return true;
        }
        return false;
    }

    //clean the box text
    private void cleanEditText() {
        etUserNameFrb.setText("");
        etUserLastNameFrb.setText("");
        etUserEmailFrb.setText("");
        etUserPasswordFrb.setText("");
    }

    //destroy objects
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
